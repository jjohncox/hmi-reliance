# Stop the ihs webserver
if [ -d /opt/websphere85/ihs ] ; then
	echo "Stopping ihs web server"
	/opt/websphere85/ihs/bin/apachectl stop 
fi

# Stop the websphere appserver and ihs server
for i in `ls /opt/websphere85/appserver/profiles/AppSrv01/config/cells/*Cell01/nodes/*Node01/servers | grep -v nodeagent | grep -v ihs`
do
        /usr/bin/su was -c "/opt/websphere85/appserver/profiles/AppSrv01/bin/stopServer.sh $i -username wasbind -password Bind4ME2"
done

# Stop the node manager
/usr/bin/su was -c "/opt/websphere85/appserver/profiles/AppSrv01/bin/stopNode.sh -username wasbind -password Bind4ME2"

# Stop the network deployment manager
/usr/bin/su was -c "/opt/websphere85/appserver/profiles/Dmgr01/bin/stopManager.sh -username wasbind -password Bind4ME2"

sleep 1
echo "--------------------------"
echo "websphere processes left:"
ps -ef | grep websphere85 | grep -v grep
exit 0
