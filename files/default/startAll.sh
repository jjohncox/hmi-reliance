ulimit -n 2048
# Start up the network deployment manager
/usr/bin/su was -c /opt/websphere85/appserver/profiles/Dmgr01/bin/startManager.sh

# Start the node manader
/usr/bin/su was -c /opt/websphere85/appserver/profiles/AppSrv01/bin/startNode.sh

# Start up websphere appserver and ihs server
for i in `ls /opt/websphere85/appserver/profiles/AppSrv01/config/cells/*Cell01/nodes/*Node01/servers | grep -v nodeagent | grep -v ihs`
do
        /usr/bin/su was -c "/opt/websphere85/appserver/profiles/AppSrv01/bin/startServer.sh $i"
done

# Start up ihs webserver
if [ -d /opt/websphere85/ihs ] ; then
	echo "Starting ihs web server"
	/opt/websphere85/ihs/bin/apachectl start
fi
