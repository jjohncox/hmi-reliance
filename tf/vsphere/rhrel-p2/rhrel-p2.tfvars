# Variables for building pleco on VSphere
#
vmname        = "rhrel-p2 - Reliance Production Server"
vm_cpus       = 3
vm_memory     = 12288
hostname      = "rhrel-p2"
datastore     = "xio_prod30"
ip_address    = "10.17.10.27"
ip_gateway    = "10.17.10.1"
vlan          = "VLAN 171 UNIX Production"
chef_env_name = "reliance-prod-p2"
