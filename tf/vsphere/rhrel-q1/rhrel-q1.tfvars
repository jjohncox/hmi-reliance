# Variables for building pleco on VSphere
#
vmname        = "rhrel-q1 - Reliance QA Server"
vm_cpus       = 4
vm_memory     = 12288
hostname      = "rhrel-q1"
datastore     = "xio_test03"
ip_address    = "10.17.10.127"
ip_gateway    = "10.17.10.1"
vlan          = "VLAN 171 UNIX Production"
chef_env_name = "reliance-qa"
