# Variables for building pleco on VSphere
#
variable "datacenter"         { default = "HMI-MAINSITE-PROD" }
variable "vmname"             { default = "Pleco - Jim J. LN Testing" }
variable "vm_folder"          { default = "UNIX/Reliance-ETQ" }
variable "vm_memory"          { default = 12288 }
variable "vm_cpus"            { default = 1 }
variable "disk_size"          { default = 80 }
variable "datastore"	      { default = "xio_test01" }
variable "hostname"           { default = "pleco" }
variable "domain"             { default = "hermanmiller.com" }
variable "ip_address"         { default = "10.17.20.25" }
variable "netmask"            { default = 24 }
variable "ip_gateway"         { default = "10.17.20.1" }
variable "vlan"		      { default = "VLAN 172 UNIX_ERP Development" }
variable "dns_server_list"    { default = ["10.16.20.200", "10.148.20.200"] }
variable "dns_suffix_list"    { default = ["hermanmiller.com"] }

# -----------------------------------------------------------------------------
# Chef variables
variable "ssh_user"           { default = "hmiadmin" }
variable "ssh_password"       { default = "hmiadmin" }
variable "chef_user_name"     { default = "jjk325" }
variable "chef_user_key_path" { default = "~/.chef/hmi-jjk325.pem" }
variable "chef_env_name"      { default = "reliance-dev" }
