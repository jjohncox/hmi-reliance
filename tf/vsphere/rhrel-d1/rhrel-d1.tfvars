# Variables for building pleco on VSphere
#
vmname        = "rhrel-d1 - Reliance Development Server"
vm_cpus       = 1
vm_memory     = 12288
hostname      = "rhrel-d1"
datastore     = "xio_dev03"
ip_address    = "10.17.20.126"
chef_env_name = "reliance-dev"
