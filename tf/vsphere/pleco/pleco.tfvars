# Variables for building pleco on VSphere
#
vmname        = "Pleco - Jim J. LN Testing"
vm_cpus       = 4
vm_memory     = 16384
hostname      = "pleco"
datastore     = "xio_prod35"
ip_address    = "10.17.20.25"
chef_env_name = "reliance-qa"
