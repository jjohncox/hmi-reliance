#
#  Terraform code to create a VSphere VM
#
# Tell terraform we're using the vsphere provider.
#  Note: the first time you use this provider on your build environment,
#        the terraform init command must be run to install this provider.
provider "vsphere" {
  # Source the env file to define environment variables for vsphere access
  #  . ./env

  # if you have a self-signed cert
  allow_unverified_ssl = true
}

# Specify the datacenter
data "vsphere_datacenter" "dc" {
  name                = "${var.datacenter}"
}

# datastore is hardcoded for now, should be passed as a variable.
data "vsphere_datastore" "datastore" {
  name                = "${var.datastore}"
  datacenter_id       = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_resource_pool" "pool" {
  name                = "REDHAT/Resources"
  datacenter_id       = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name 		      = "${var.vlan}"
  datacenter_id       = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name                = "UNIX/Templates/RH74"
  datacenter_id       = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_virtual_machine" "vm" {
  name                = "${var.vmname}"
  resource_pool_id    = "${data.vsphere_resource_pool.pool.id}"
  datastore_id        = "${data.vsphere_datastore.datastore.id}"
  folder              = "${var.vm_folder}"

  num_cpus            = "${var.vm_cpus}"
  memory              = "${var.vm_memory}"
  guest_id            = "${data.vsphere_virtual_machine.template.guest_id}"

  network_interface {
    network_id        = "${data.vsphere_network.network.id}"
  }

  disk {
    label             = "disk0"
    size              = "${var.disk_size}"
  }

  clone {
    template_uuid     = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name       = "${var.hostname}"
        domain          = "${var.domain}"
      }

      network_interface {
        ipv4_address    = "${var.ip_address}"
        ipv4_netmask    = "${var.netmask}"
      }

      ipv4_gateway      = "${var.ip_gateway}"

      dns_server_list   = "${var.dns_server_list}"
      dns_suffix_list   = "${var.dns_suffix_list}"
    }
  }

  # Wait for the instance to be created
  provisioner "local-exec" {
    command = "sleep 45"
  }

  # The Chef provisioner will install Chef, register this instance
  # with the Chef server, and run the Chef client with the run list
  # below.
  provisioner "chef" {
    attributes_json         = <<-EOF
      {
        "set_fqdn": "${var.hostname}.hermanmiller.com"
      }
    EOF

    connection {
      type            = "ssh"
      user            = "${var.ssh_user}"
      password        = "${var.ssh_password}"
    }

    disable_reporting       = true
    environment             = "${var.chef_env_name}"
    log_to_file             = true
    node_name               = "${var.hostname}.hermanmiller.com"
    recreate_client         = true

    run_list = [
      "role[base]",
      "role[reliance]"
    ]

    skip_install            = false
    skip_register           = false
    server_url              = "https://rhchef-p1.hermanmiller.com/organizations/hmi"
    user_name               = "${var.chef_user_name}"
    user_key                = "${file(var.chef_user_key_path)}"
    ssl_verify_mode         = ":verify_none"
  }
}
