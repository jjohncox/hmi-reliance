# vol_size: logical volume size to create
default['was']['vol_size']         = '16G'
default['was']['vol_group']        = 'rootvg'
default['hmi-reliance']['media_location'] = 'unix-nas:software'

# NFS filesystems to be mounted
#
# These will usually be different for all environments
# and should be defined in the ennvironments file

default['was']['nfs-hash']         = {
	"unix-nas:/uf_p01_waslogs": "/nas/waslogs"
}

