#
# Cookbook Name:: hmi-reliance
# Recipe:: default
#
# Copyright 2018, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'lvm::default'

#----------------------------------------------------------------
# Create group and user
# A local group with id 300 is needed prior to creating the was passwd
# file entry.

group 'was' do
  gid           300
end

user 'was' do
  uid           300
  gid           300
  shell         '/usr/bin/ksh'
  password      '*'
  home          '/opt/websphere85'
  comment       'Websphere User'
end

#----------------------------------------------------------------
# Create directory for Websphere file system
directory "/opt/websphere85" do
  mode          '0755'
  owner         'was'
  group         'was'
end

# Create separate Websphere file system
lvm_logical_volume 'was' do
  group         node['was']['vol_group']
  size          node['was']['vol_size']
  filesystem    'ext4'
  mount_point   "/opt/websphere85"
end

# Create the base /nas directory and then mount all needed filesystems
# under it.  These filesystems are specified in a hash in the attributes
# file and will vary by environment
directory '/nas' do
  owner 'root'
  group 'root'
  mode 0755
  action :create
end

node['was']['nfs-hash'].each do |nas_volume, mount_dir|
  directory "#{mount_dir}" do
    path                "#{mount_dir}"
    recursive           true
    mode                '0755'
    owner               300
    group               300
  end

  mount "#{mount_dir}" do
    enabled             true
    fstype              'nfs'
    device              "#{nas_volume}"
    mount_point         "#{mount_dir}"
    options             'rw,soft,bg,vers=3,proto=tcp,rsize=65536,wsize=65536'
    action              [:mount, :enable]
    not_if 'df | grep #{mount_dir}'
  end
end
