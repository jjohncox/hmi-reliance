#
# Cookbook Name:: hmi-reliance
# Recipe:: websphere
#
# Copyright 2018, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Mount the software repository
mount '/mnt' do
  fstype              'nfs'
  device              node['hmi-reliance']['media_location']
  options             'ro,soft,bg,vers=3,proto=tcp,rsize=65536,wsize=65536'
  action              :mount
end

# Install the installation manager
install_mgr 'ibm-im install' do
  install_package '/mnt/was85/InstallationManager-1.8.9-LinuxX64.zip'
  package_name 'com.ibm.cic.agent'
  ibm_root_dir '/opt/websphere85'
  service_user 'root'
  service_group 'root'
end

# Install Websphere Network Deployment

execute 'Install WebSphere ND' do
  command "/opt/websphere85/InstallationManager/eclipse/IBMIM -acceptLicense -nosplash -silent --launcher.ini silent-install.ini -input /mnt/was85/was-fp11-java8.xml -log /var/IBM/InstallationManager/logs/ND.log"
end

hostname = "#{node['hostname']}"

# Create a Deployment Manager profile

if node['was']['no-dmgr'] != 'true'
  execute 'Create Deployment Manager' do
    command "/opt/websphere85/appserver/bin/manageprofiles.sh -create -profileName Dmgr01 -profilePath /opt/websphere85/appserver/profiles/Dmgr01 -templatePath /opt/websphere85/appserver/profileTemplates/management -cellName #{hostname}Cell01 -hostName #{hostname}.hermanmiller.com -nodeName #{hostname}CellManager01"
  end
end

# Create an Application Server profile
execute 'Create App Server Profile' do
  command "/opt/websphere85/appserver/bin/manageprofiles.sh -create -profileName AppSrv01 -profilePath /opt/websphere85/appserver/profiles/AppSrv01 -templatePath /opt/websphere85/appserver/profileTemplates/managed -cellName #{hostname}01 -hostName #{hostname}.hermanmiller.com -nodeName #{hostname}Node01"
end

# Change ownership of /opt/websphere85 to was:was

execute 'Change ownership of /opt/websphere85/appserver' do
  command "chown -R was:was /opt/websphere85/appserver"
end

# Fix the log files to go to the NA

directory "/nas/waslogs/#{hostname}" do
  user 'was'
  group 'was'
end

if node['was']['no-dmgr'] != 'true'
  directory "/nas/waslogs/#{hostname}/dmgr" do
    user 'was'
    group 'was'
  end
end

directory '/opt/websphere85/appserver/logs' do
  recursive true
  action :delete
end

directory '/opt/websphere85/appserver/profiles/AppSrv01/logs' do
  recursive true
  action :delete
end

if node['was']['no-dmgr'] != 'true'
  directory '/opt/websphere85/appserver/profiles/Dmgr01/logs/dmgr' do
    recursive true
    action :delete
  end
end

link '/opt/websphere85/appserver/profiles/AppSrv01/logs' do
  to "/nas/waslogs/#{hostname}"
  owner 'was'
  group 'was'
end

link '/opt/websphere85/appserver/logs' do
  to "/nas/waslogs/#{hostname}"
  owner 'was'
  group 'was'
end

if node['was']['no-dmgr'] != 'true'
  link '/opt/websphere85/appserver/profiles/Dmgr01/logs/dmgr' do
    to "/nas/waslogs/#{hostname}/dmgr"
    owner 'was'
    group 'was'
  end
end

# Create the stopAll.sh and startAll.sh scripts
cookbook_file '/opt/websphere85/startAll.sh' do
  source 'startAll.sh'
  owner 'was'
  group 'was'
  mode 0755
  action :create
end

cookbook_file '/opt/websphere85/stopAll.sh' do
  source 'stopAll.sh'
  owner 'was'
  group 'was'
  mode 0755
  action :create
end

# Move in the java database drivers
directory "/opt/websphere85/DBdrivers" do
  user 'was'
  group 'was'
end

cookbook_file '/opt/websphere85/DBdrivers/ojdbc6.jar' do
  source 'ojdbc6.jar'
  owner 'was'
  group 'was'
  mode 0644
  action :create
end

cookbook_file '/opt/websphere85/DBdrivers/ojdbc8.jar' do
  source 'ojdbc8.jar'
  owner 'was'
  group 'was'
  mode 0644
  action :create
end

cookbook_file '/opt/websphere85/DBdrivers/sqljdbc42.jar' do
  source 'sqljdbc42.jar'
  owner 'was'
  group 'was'
  mode 0644
  action :create
end

# Federate the deployment manager and application server
if node['was']['no-dmgr'] == 'true'
  execute 'Federate Application Server' do
    command "/opt/websphere85/appserver/bin/addNode.sh #{node['was']['dmgr-host']}.hermanmiller.com -conntype soap -profileName AppSrv01 -includeapps"
    user  'was'
    group 'was'
  end
else
  execute 'Start deployment manager' do
    command "/opt/websphere85/appserver/profiles/Dmgr01/bin/startManager.sh"
    user 'was'
    group 'was'
  end

  execute 'Federate Application Server' do
    command "/opt/websphere85/appserver/bin/addNode.sh #{hostname}.hermanmiller.com -conntype soap -profileName AppSrv01 -includeapps"
    user  'was'
    group 'was'
  end
end

# The environment can now be accesses at http://hostname:9060/admin
if node['was']['no-plugins'] != 'true'
  execute 'Install IHS and Plugins' do
    command "/opt/websphere85/InstallationManager/eclipse/IBMIM -acceptLicense -nosplash -silent --launcher.ini silent-install.ini -input /mnt/was85/was-ihs.xml"
  end
end

mount '/mnt' do
  fstype              'nfs'
  device              node['hmi-was']['media_location']
  action              :unmount
end
