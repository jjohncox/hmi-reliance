name             'hmi-reliance'
maintainer       'YOUR_COMPANY_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures hmi-reliance'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends          'lvm',               '~> 4.1.10'
depends		 'ibm-was-installer', '~> 1.2.7'
# depends          'websphere',         '~> 1.0.2'
